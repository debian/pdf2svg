% pdf2baresvg(1) | User Commands
%
% "February 19 2024"


# NAME

pdf2baresvg - PDF to bare SVG convertor

# SYNOPSIS

**pdf2baresvg** pdffile svgfile [page number]

# DESCRIPTION

**pdf2baresvg** launches the command pdf2svg and simplifies its output.

pdf2svg  is a tiny command-line utility using Cairo and Poppler to convert PDF
documents into SVG files. Its ouput is a standalone SVG file which comes with
an XML header.

In some cases, the XML header is not useful; for instance when one wants
to embed the SVG output as an element in some HTML page.

# OPTIONS

**[some page label]**
:    The  third  parameter is optional and serves as a page selector. 
If omitted it defaults to the first page of the passed PDF.  If passed
it must be a valid page label (typically it is a value such as "iii"
or "3").

**all**
:    This special selector causes the program to iterate over all pages
in the PDF.  Because it cannot save multiple pages into one single SVG
the second parameter is expected to contain a sensible file specification:
	   
:    **pdf2baresvg** document.pdf output-page%d.svg all

:    This selector is not encouraged for use with **pdf2baresvg**
since the XML header will not be removed from output files.

**\-h** or **\-\-help**
:    Show this message and exit.

# SEE ALSO

**pdf2svg** (1)

# AUTHOR

Georges Khaznadar <georgesk@debian.org>
:   Wrote this manpage for the Debian system.

# COPYRIGHT

Copyright © 2024 Georges Khaznadar

This manual page was written for the Debian system (and may be used by
others).

This manual page is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation as version 2 of the License.

This manual page is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>

On Debian systems, the complete text of the GNU General
Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
